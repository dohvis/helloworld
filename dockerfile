FROM ubuntu:16.04 

RUN apt-get -qq update && apt-get install -y -qq sudo && rm -rf /var/lib/apt/lists/*
# https://github.com/tianon/docker-brew-ubuntu-core/issues/48#issuecomment-215522746

RUN apt-get -qq update
RUN apt-get install -y software-properties-common python3-software-properties
# for add-apt-repository

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get -qq update && apt-get install -y -qq git python3.6 python3.6-dev wget

RUN apt-get install -y -qq build-essential libssl-dev libffi-dev python-dev
# for pycrypto

RUN wget https://bootstrap.pypa.io/get-pip.py -q
RUN python3.6 ./get-pip.py > /dev/null

ENV PYTHONUNBUFFERED 1
# 파이썬 출력 버퍼 해제

RUN mkdir /helloworld 
# docker 내에서 /helloworld 라는 이름의 폴더 생성

WORKDIR /helloworld 
# docker 내에서 코드를 실행할 폴더 위치를 /helloworld 로 지정

ADD requirements.txt /helloworld/ 
# 로컬의 requirements.txt 파일을 docker /helloworld/ 폴더로 마운트

RUN pip install -r requirements.txt 
# docker 내 requirements.txt 파일을 이용하여 패키지 설치

ADD . /helloworld/ 
# 로컬 내 현재 위치에 있는 모든 파일 및 폴더를 docker 의 /helloworld/ 폴더로 마운트

EXPOSE 8000
CMD python3.6 /helloworld/manage.py runserver 0.0.0.0:8000
